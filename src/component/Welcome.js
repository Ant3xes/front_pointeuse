// src/components/Welcome.js

import React from 'react';

function Welcome() {
  return (
    <div>
      <h1>Bienvenue dans mon application React</h1>
      <p>C'est un exemple simple pour afficher quelque chose.</p>
    </div>
  );
}

export default Welcome;
