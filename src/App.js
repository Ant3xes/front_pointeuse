import React from 'react';
import ReactDOM from 'react-dom';

// Définissez votre composant principal
function App() {

  return (
    <div>
      <h1>Mon Application React</h1>
      <p>Bienvenue dans mon application React!</p>
    </div>
  );
}

// Rendez votre composant principal dans l'élément avec l'ID "root" du fichier HTML
ReactDOM.render(<App />, document.getElementById('root'));

export default App